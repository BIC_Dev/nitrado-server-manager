package controllers

import (
	"encoding/json"
	"fmt"
	"net/http"

	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/viewmodels"
)

// UpdateAllGuilds updates the configs for all guilds
func (c *Controller) UpdateAllGuilds(w http.ResponseWriter, r *http.Request) {
	guildConfig, gErr := filestore.GetAllGuildConfigs(c.Config.AWS.Region, c.Config.AWS.S3Bucket, c.Config.AWS.S3Prefix)

	var failures []viewmodels.GuildsFailedInfo
	var updates []int64

	for _, anErr := range gErr {
		failures = append(failures, viewmodels.GuildsFailedInfo{
			Error:   anErr.Error(),
			Message: anErr.GetMessage(),
		})
	}

	for guildID, guild := range guildConfig.Guilds {
		c.GuildConfig.Guilds[guildID] = guild

		updates = append(updates, guildID)
	}

	if len(failures) > 0 {
		SendJSONResponse(w, viewmodels.UpdateAllGuildsResponse{
			Status:        "error",
			Message:       "One or more guilds failed to load",
			GuildsUpdated: updates,
			GuildsFailed:  failures,
		}, http.StatusOK)
		return
	}

	SendJSONResponse(w, viewmodels.UpdateAllGuildsResponse{
		Status:        "success",
		Message:       "All guilds updates successfully",
		GuildsUpdated: updates,
		GuildsFailed:  failures,
	}, http.StatusOK)
	return
}

// UpdateGuilds updates the configs for all guilds
func (c *Controller) UpdateGuilds(w http.ResponseWriter, r *http.Request) {
	var body viewmodels.UpdateGuildsRequestBody
	dcErr := json.NewDecoder(r.Body).Decode(&body)

	if dcErr != nil {
		c.Log.Log(fmt.Sprintf("ERROR: Could not decode update guilds body: %s", dcErr.Error()), c.Log.LogLow)
		SendJSONResponse(w, viewmodels.NewErrorResponse(dcErr.Error(), "Could not decode update guilds body"), http.StatusBadRequest)
		return
	}

	var failures []viewmodels.GuildsFailedInfo
	var updates []int64

	for _, guildID := range body.GuildIDs {
		guild, gErr := filestore.GetGuildConfig(c.Config.AWS.Region, c.Config.AWS.S3Bucket, c.Config.AWS.S3Prefix, guildID)

		if gErr != nil {
			failures = append(failures, viewmodels.GuildsFailedInfo{
				Error:   gErr.Error(),
				Message: gErr.GetMessage(),
			})

			continue
		}

		c.GuildConfig.Guilds[guild.Guild.ID] = *guild

		updates = append(updates, guild.Guild.ID)
	}

	if len(failures) > 0 {
		SendJSONResponse(w, viewmodels.UpdateGuildsResponse{
			Status:        "error",
			Message:       "One or more guilds failed to load",
			GuildsUpdated: updates,
			GuildsFailed:  failures,
		}, http.StatusOK)
		return
	}

	SendJSONResponse(w, viewmodels.UpdateGuildsResponse{
		Status:        "success",
		Message:       "Specified guilds updated successfully",
		GuildsUpdated: updates,
		GuildsFailed:  failures,
	}, http.StatusOK)
	return
}
