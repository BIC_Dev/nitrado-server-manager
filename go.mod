module gitlab.com/BIC_Dev/nitrado-server-manager

go 1.14

require (
	github.com/aws/aws-sdk-go v1.33.17
	github.com/bwmarrin/discordgo v0.22.0
	github.com/gammazero/workerpool v1.0.0
	github.com/gorilla/mux v1.7.4
	github.com/jinzhu/copier v0.0.0-20190924061706-b57f9002281a
	github.com/jinzhu/gorm v1.9.15
	gopkg.in/yaml.v2 v2.3.0
)
