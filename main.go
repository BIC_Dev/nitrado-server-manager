package main

import (
	"fmt"
	"log"
	"os"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/controllers"
	"gitlab.com/BIC_Dev/nitrado-server-manager/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager/routes"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/handler"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/runners"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// Service information for the service
type Service struct {
	Config *utils.Config
	Log    *utils.Log
	DG     *discordgo.Session
}

func main() {
	env := os.Getenv("ENVIRONMENT")
	logLevel, err := strconv.Atoi(os.Getenv("LOG_LEVEL"))

	if err != nil {
		log.Fatal(err.Error())
	}

	service := Service{
		Config: utils.GetConfig(env),
		Log:    utils.InitLog(logLevel),
	}

	guildConfig, gcErr := filestore.GetAllGuildConfigs(service.Config.AWS.Region, service.Config.AWS.S3Bucket, service.Config.AWS.S3Prefix)

	for _, anErr := range gcErr {
		service.Log.Log(fmt.Sprintf("ERROR: %s: %s", anErr.GetMessage(), anErr.Error()), service.Log.LogHigh)
	}

	if os.Getenv("MIGRATE") == "TRUE" {
		service.migrateTables(models.PotentialBan{}, models.PotentialUnban{}, models.OnlinePlayersMessage{}, models.PotentialRestartGameserver{}, models.PotentialStopGameserver{})
	}

	discordToken := os.Getenv("DISCORD_TOKEN")

	dg, discErr := discordgo.New("Bot " + discordToken)

	if discErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Error creating Discord session: %s", discErr.Error()), service.Log.LogFatal)
		return
	}

	defer dg.Close()

	service.DG = dg

	// Open a websocket connection to Discord and begin listening.
	openErr := dg.Open()
	if openErr != nil {
		service.Log.Log(fmt.Sprintf("ERROR: Error opening connection: %s", openErr.Error()), service.Log.LogFatal)
		return
	}

	dh := handler.DiscordHandler{
		Config:                service.Config,
		Log:                   service.Log,
		DG:                    service.DG,
		NitradoV2ServiceToken: os.Getenv("NITRADO_V2_SERVICE_TOKEN"),
		GuildConfig:           guildConfig,
	}

	runner := runners.Runner{
		Config:                service.Config,
		Log:                   service.Log,
		DG:                    service.DG,
		NitradoV2ServiceToken: os.Getenv("NITRADO_V2_SERVICE_TOKEN"),
		GuildConfig:           guildConfig,
	}

	// Wait here until CTRL-C or other term signal is received.
	service.Log.Log("PROCESS: Bot is now running", service.Log.LogInformation)

	service.Log.Log("PROCESS: Set up Discord handlers", service.Log.LogInformation)
	dh.SetupHandlers()

	service.Log.Log("PROCES: Started background processes", service.Log.LogInformation)
	runner.StartBackgroundProcesses()

	controller := controllers.Controller{
		Config:       service.Config,
		Log:          service.Log,
		ServiceToken: os.Getenv("SERVICE_TOKEN"),
		GuildConfig:  guildConfig,
	}

	router := routes.GetRouter()
	routes.AddRoutes(router, &controller)

	service.Log.Log("SUCCESS: Set up router", service.Log.LogInformation)

	service.Log.Log("PROCESS: Starting MUX listener", service.Log.LogInformation)
	routes.StartListener(router, os.Getenv("LISTENER_PORT"))

}

func (s *Service) migrateTables(tables ...interface{}) []*utils.ModelError {
	s.Log.Log("Migrating tables: potential_bans, potential_unbans, online_player_message, potential_restart_gameserver, potential_stop_gameserver", s.Log.LogInformation)
	var migrationErrors []*utils.ModelError

	dbStruct, dbErr := db.GetDB(s.Config)

	if dbErr != nil {
		s.Log.Log(fmt.Sprintf("%s: %s", dbErr.GetMessage(), dbErr.Error()), s.Log.LogFatal)
	}

	defer dbStruct.GetDB().Close()

	for _, table := range tables {
		migrationError := dbStruct.Migrate(table)

		if migrationError != nil {
			migrationErrors = append(migrationErrors, migrationError)
		}
	}

	if len(migrationErrors) > 0 {
		return migrationErrors
	}

	return nil
}
