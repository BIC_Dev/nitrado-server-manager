package db

import (
	"fmt"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"

	// Loading Postgres dialect
	_ "github.com/jinzhu/gorm/dialects/postgres"
)

// PostgreSQL struct
type PostgreSQL struct {
	DB       *gorm.DB
	Username string
	Password string
}

// Connect starts initial connection to DB
func (pg *PostgreSQL) Connect(c *utils.Config) *utils.ModelError {
	connectionString := fmt.Sprintf("host=%s port=%s user=%s dbname=%s password=%s", c.PostgreSQL.Host, c.PostgreSQL.Port, pg.Username, c.PostgreSQL.DBName, pg.Password)
	newDB, err := gorm.Open("postgres", connectionString)

	if err != nil {
		modelError := utils.NewModelError(err)
		modelError.SetMessage("Failed to connect to PostgreSQL DB")
		modelError.SetStatus(500)

		return modelError
	}

	pg.DB = newDB

	return nil
}

// Migrate migrates a table
func (pg *PostgreSQL) Migrate(table interface{}) *utils.ModelError {
	pg.DB.AutoMigrate(table)

	return nil
}

// GetDB gets the DB from the struct
func (pg *PostgreSQL) GetDB() *gorm.DB {
	return pg.DB
}
