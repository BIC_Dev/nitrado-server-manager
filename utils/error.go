package utils

// APIError interface
type APIError interface {
	Error() string
	GetStatus() int
	SetStatus(int)
	GetMessage() string
	SetMessage(string)
}

// RequestError request error struct
type RequestError struct {
	StatusCode int
	Err        error
	Message    string
}

// ApplicationError application error struct
type ApplicationError struct {
	StatusCode int
	Err        error
	Message    string
}

// ModelError model error struct
type ModelError struct {
	StatusCode int
	Err        error
	Message    string
}

// ValidationError validation error struct
type ValidationError struct {
	StatusCode int
	Err        error
	Message    string
}

// CacheError cache error struct
type CacheError struct {
	StatusCode int
	Err        error
	Message    string
}

// ServiceError service error struct
type ServiceError struct {
	StatusCode int
	Err        error
	Message    string
}

func (r *RequestError) Error() string {
	return r.Err.Error()
}

// GetStatus status code for the error
func (r *RequestError) GetStatus() int {
	return r.StatusCode
}

// SetStatus sets the status code for the error
func (r *RequestError) SetStatus(status int) {
	r.StatusCode = status
}

// GetMessage details of the error
func (r *RequestError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *RequestError) SetMessage(message string) {
	r.Message = message
}

// NewRequestError generates a new RequestError
func NewRequestError(err error) *RequestError {
	return &RequestError{
		Err: err,
	}
}

func (r *ApplicationError) Error() string {
	return r.Err.Error()
}

// GetStatus status code for the error
func (r *ApplicationError) GetStatus() int {
	return r.StatusCode
}

// SetStatus sets the status code for the error
func (r *ApplicationError) SetStatus(status int) {
	r.StatusCode = status
}

// GetMessage details of the error
func (r *ApplicationError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *ApplicationError) SetMessage(message string) {
	r.Message = message
}

// NewApplicationError generates a new ApplicationError
func NewApplicationError(err error) *ApplicationError {
	return &ApplicationError{
		Err: err,
	}
}

func (r *ModelError) Error() string {
	return r.Err.Error()
}

// GetStatus status code for the error
func (r *ModelError) GetStatus() int {
	return r.StatusCode
}

// SetStatus sets the status code for the error
func (r *ModelError) SetStatus(status int) {
	r.StatusCode = status
}

// GetMessage details of the error
func (r *ModelError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *ModelError) SetMessage(message string) {
	r.Message = message
}

// NewModelError generates a new ModelError
func NewModelError(err error) *ModelError {
	return &ModelError{
		Err: err,
	}
}

func (r *ValidationError) Error() string {
	return r.Err.Error()
}

// GetStatus status code for the error
func (r *ValidationError) GetStatus() int {
	return r.StatusCode
}

// SetStatus sets the status code for the error
func (r *ValidationError) SetStatus(status int) {
	r.StatusCode = status
}

// GetMessage details of the error
func (r *ValidationError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *ValidationError) SetMessage(message string) {
	r.Message = message
}

// NewValidationError generates a new ValidationError
func NewValidationError(err error) *ValidationError {
	return &ValidationError{
		Err: err,
	}
}

func (r *CacheError) Error() string {
	return r.Err.Error()
}

// GetStatus status code for the error
func (r *CacheError) GetStatus() int {
	return r.StatusCode
}

// SetStatus sets the status code for the error
func (r *CacheError) SetStatus(status int) {
	r.StatusCode = status
}

// GetMessage details of the error
func (r *CacheError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *CacheError) SetMessage(message string) {
	r.Message = message
}

// NewCacheError generates a new CacheError
func NewCacheError(err error) *CacheError {
	return &CacheError{
		Err: err,
	}
}

func (r *ServiceError) Error() string {
	return r.Err.Error()
}

// GetStatus status code for the error
func (r *ServiceError) GetStatus() int {
	return r.StatusCode
}

// SetStatus sets the status code for the error
func (r *ServiceError) SetStatus(status int) {
	r.StatusCode = status
}

// GetMessage details of the error
func (r *ServiceError) GetMessage() string {
	return r.Message
}

// SetMessage sets the details map of the error
func (r *ServiceError) SetMessage(message string) {
	r.Message = message
}

// NewServiceError generates a new ServiceError
func NewServiceError(err error) *ServiceError {
	return &ServiceError{
		Err: err,
	}
}
