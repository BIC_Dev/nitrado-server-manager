package utils

import (
	"log"
	"os"

	"gopkg.in/yaml.v2"
)

// Config struct that contians the structure of the config
type Config struct {
	DB struct {
		Type string `yaml:"type"`
	} `yaml:"DB"`
	SQLite3 struct {
		Path   string `yaml:"path"`
		DBName string `yaml:"db_name"`
	} `yaml:"SQLITE3"`
	PostgreSQL struct {
		Host   string `yaml:"host"`
		Port   string `yaml:"port"`
		DBName string `yaml:"db_name"`
	} `yaml:"POSTGRESQL"`
	Runners struct {
		PlayersInterval int `yaml:"players_interval"`
		PlayersWorkers  int `yaml:"players_workers"`
		LogsInterval    int `yaml:"logs_interval"`
		LogsWorkers     int `yaml:"logs_workers"`
	} `yaml:"RUNNERS"`
	Bot struct {
		TitleURL   string             `yaml:"title_url"`
		ErrorColor int                `yaml:"error_color"`
		Commands   map[string]Command `yaml:"commands"`
	} `yaml:"BOT"`
	NitradoService NitradoService `yaml:"NITRADO_SERVICE"`
	AWS            struct {
		Region   string `yaml:"region"`
		S3Bucket string `yaml:"s3_bucket"`
		S3Prefix string `yaml:"s3_prefix"`
	} `yaml:"AWS"`
}

// Command struct
type Command struct {
	Name        string              `yaml:"name"`
	MinArgs     int                 `yaml:"min_args"`
	MaxArgs     int                 `yaml:"max_args"`
	Color       int                 `yaml:"color"`
	Reactions   map[string]Reaction `yaml:"reactions"`
	Description string              `yaml:"description"`
	Examples    []string            `yaml:"examples"`
}

// Reaction struct
type Reaction struct {
	ID   string `yaml:"id"`
	Icon string `yaml:"icon"`
}

// NitradoService struct
type NitradoService struct {
	BaseURL string `yaml:"base_url"`
}

// GetConfig gets the config file and returns a Config struct
func GetConfig(env string) *Config {
	configFile := "./configs/conf-" + env + ".yml"
	f, err := os.Open(configFile)

	if err != nil {
		log.Fatal("Missing config file at path: " + configFile)
	}

	defer f.Close()

	var config Config
	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&config)

	if err != nil {
		log.Fatal("Could not parse config file")
	}

	return &config
}
