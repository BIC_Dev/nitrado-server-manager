package nitrado

import (
	"fmt"

	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// StopGameserverResponse struct
type StopGameserverResponse struct {
	Status     string               `json:"status"`
	Message    string               `json:"message"`
	Gameserver filestore.Gameserver `json:"-"`
}

// StopGameserverBody struct
type StopGameserverBody struct {
	Message     string `json:"message"`
	StopMessage string `json:"stop_message"`
}

// StopGameserverError struct
type StopGameserverError struct {
	Gameserver filestore.Gameserver
	Error      *utils.ServiceError
}

// RestartGameserverResponse struct
type RestartGameserverResponse struct {
	Status     string               `json:"status"`
	Message    string               `json:"message"`
	Gameserver filestore.Gameserver `json:"-"`
}

// RestartGameserverBody struct
type RestartGameserverBody struct {
	Message        string `json:"message"`
	RestartMessage string `json:"stop_message"`
}

// RestartGameserverError struct
type RestartGameserverError struct {
	Gameserver filestore.Gameserver
	Error      *utils.ServiceError
}

// GetBanlistResponse struct
type GetBanlistResponse struct {
	Status     string               `json:"status"`
	Message    string               `json:"message"`
	Players    []Player             `json:"players"`
	Gameserver filestore.Gameserver `json:"-"`
}

// GetBanlistError struct
type GetBanlistError struct {
	Gameserver filestore.Gameserver
	Error      *utils.ServiceError
}

// StopGameserver func
func (n *NitradoRequest) StopGameserver(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/gameserver/%d/stop", n.Config.NitradoService.BaseURL, rd.Gameserver.ID)

	body := StopGameserverBody{
		Message:     "Service request to stop gameserver",
		StopMessage: "Stopping server momentarily",
	}

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.AccountName,
	}

	var response StopGameserverResponse

	rErr := Request(&response, "POST", url, nil, body, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Status:     response.Status,
		Message:    response.Message,
		Gameserver: rd.Gameserver,
	}
	return
}

// RestartGameserver func
func (n *NitradoRequest) RestartGameserver(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/gameserver/%d/restart", n.Config.NitradoService.BaseURL, rd.Gameserver.ID)

	body := RestartGameserverBody{
		Message:        "Service request to restart gameserver",
		RestartMessage: "Restarting server momentarily",
	}

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.AccountName,
	}

	var response RestartGameserverResponse

	rErr := Request(&response, "POST", url, nil, body, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Status:     response.Status,
		Message:    response.Message,
		Gameserver: rd.Gameserver,
	}
	return
}

// GetBanlist func
func (n *NitradoRequest) GetBanlist(rd RequestData, params map[string]string, resp chan *RequestResponse, err chan *RequestError) {
	url := fmt.Sprintf("%s/gameserver/%d/banlist", n.Config.NitradoService.BaseURL, rd.Gameserver.ID)

	headers := map[string]string{
		"Service-Token": n.NitradoV2ServiceToken,
		"Account-Name":  rd.Gameserver.AccountName,
	}

	var response GetBanlistResponse

	rErr := Request(&response, "GET", url, nil, nil, headers)

	if rErr != nil {
		err <- &RequestError{
			Gameserver: rd.Gameserver,
			Error:      rErr,
		}
		return
	}

	resp <- &RequestResponse{
		Status:     response.Status,
		Message:    response.Message,
		Players:    response.Players,
		Gameserver: rd.Gameserver,
	}

	return
}
