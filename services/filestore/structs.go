package filestore

// GuildConfig struct
type GuildConfig struct {
	Guilds map[int64]Guild
}

// Guild struct
type Guild struct {
	Guild struct {
		ID          int64  `yaml:"id"`
		Name        string `yaml:"name"`
		ContactID   int64  `yaml:"contact_id"`
		ContactName string `yaml:"contact_name"`
	} `yaml:"GUILD"`
	Bot struct {
		Prefix     string             `yaml:"prefix"`
		ErrorColor int                `yaml:"error_color"`
		Commands   map[string]Command `yaml:"commands"`
	} `yaml:"BOT"`
	Gameservers []Gameserver `yaml:"GAMESERVERS"`
}

// Command struct
type Command struct {
	Roles []int64 `yaml:"roles"`
	Color int     `yaml:"color"`
}

// Gameserver struct
type Gameserver struct {
	ID          int    `yaml:"id"`
	Name        string `yaml:"name"`
	ChatLog     Runner `yaml:"chat_log"`
	AdminLog    Runner `yaml:"admin_log"`
	PlayerList  Runner `yaml:"player_list"`
	AccountName string `yaml:"account_name"`
}

// Runner struct
type Runner struct {
	Enabled   bool  `yaml:"enabled"`
	ChannelID int64 `yaml:"channel_id"`
}
