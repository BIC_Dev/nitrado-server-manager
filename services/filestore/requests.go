package filestore

import (
	"fmt"
	"io/ioutil"
	"net/http"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gopkg.in/yaml.v2"
)

// GetAllGuildConfigs func
func GetAllGuildConfigs(region string, bucket string, prefix string) (*GuildConfig, []*utils.ServiceError) {
	sess, sErr := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)

	var errors []*utils.ServiceError

	if sErr != nil {
		serviceError := utils.NewServiceError(sErr)
		serviceError.SetMessage("Failed to create AWS session")
		serviceError.SetStatus(http.StatusFailedDependency)

		errors = append(errors, serviceError)

		return nil, errors
	}

	// Create S3 service client
	svc := s3.New(sess)

	resp, lErr := svc.ListObjectsV2(&s3.ListObjectsV2Input{
		Bucket: aws.String(bucket),
		Prefix: aws.String(prefix),
	})

	if lErr != nil {
		serviceError := utils.NewServiceError(lErr)
		serviceError.SetMessage(fmt.Sprintf("Failed to list objects in bucket: %s", bucket))
		serviceError.SetStatus(http.StatusFailedDependency)

		errors = append(errors, serviceError)

		return nil, errors
	}

	var guilds GuildConfig

	guilds.Guilds = make(map[int64]Guild)

	for _, item := range resp.Contents {
		obj, gErr := svc.GetObject(&s3.GetObjectInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(*item.Key),
		})

		if gErr != nil {
			serviceError := utils.NewServiceError(gErr)
			serviceError.SetMessage(fmt.Sprintf("Failed to get s3 object: %s", *item.Key))
			serviceError.SetStatus(http.StatusFailedDependency)

			errors = append(errors, serviceError)

			continue
		}

		s3objectBytes, rErr := ioutil.ReadAll(obj.Body)

		if rErr != nil {
			serviceError := utils.NewServiceError(rErr)
			serviceError.SetMessage(fmt.Sprintf("Failed to read s3 object: %s", *item.Key))
			serviceError.SetStatus(http.StatusFailedDependency)

			errors = append(errors, serviceError)

			continue
		}

		var guild Guild

		yErr := yaml.Unmarshal(s3objectBytes, &guild)

		if yErr != nil {
			serviceError := utils.NewServiceError(yErr)
			serviceError.SetMessage(fmt.Sprintf("Failed to convert s3 object to struct: %s", *item.Key))
			serviceError.SetStatus(http.StatusFailedDependency)

			errors = append(errors, serviceError)

			continue
		}

		guilds.Guilds[guild.Guild.ID] = guild
	}

	delete(guilds.Guilds, 0)

	return &guilds, errors
}

// GetGuildConfig func
func GetGuildConfig(region string, bucket string, prefix string, guildID int64) (*Guild, *utils.ServiceError) {
	sess, sErr := session.NewSession(&aws.Config{
		Region: aws.String(region)},
	)

	if sErr != nil {
		serviceError := utils.NewServiceError(sErr)
		serviceError.SetMessage("Failed to create AWS session")
		serviceError.SetStatus(http.StatusFailedDependency)

		return nil, serviceError
	}

	key := fmt.Sprintf("%s%d.yml", prefix, guildID)

	// Create S3 service client
	svc := s3.New(sess)

	obj, gErr := svc.GetObject(&s3.GetObjectInput{
		Bucket: aws.String(bucket),
		Key:    aws.String(key),
	})

	if gErr != nil {
		serviceError := utils.NewServiceError(gErr)
		serviceError.SetMessage(fmt.Sprintf("Failed to get s3 object: %s", key))
		serviceError.SetStatus(http.StatusFailedDependency)

		return nil, serviceError
	}

	s3objectBytes, rErr := ioutil.ReadAll(obj.Body)

	if rErr != nil {
		serviceError := utils.NewServiceError(rErr)
		serviceError.SetMessage(fmt.Sprintf("Failed to read s3 object: %s", key))
		serviceError.SetStatus(http.StatusFailedDependency)

		return nil, serviceError
	}

	var guild Guild

	yErr := yaml.Unmarshal(s3objectBytes, &guild)

	if yErr != nil {
		serviceError := utils.NewServiceError(yErr)
		serviceError.SetMessage(fmt.Sprintf("Failed to convert s3 object to struct: %s", key))
		serviceError.SetStatus(http.StatusFailedDependency)

		return nil, serviceError
	}

	return &guild, nil
}
