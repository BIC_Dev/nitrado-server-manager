package discordapi

import (
	"encoding/json"
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// CreatePost func
func (d *Discord) CreatePost(channelID string, content string, embed *discordgo.MessageEmbed) (*discordgo.Message, *utils.ServiceError) {
	if embed != nil {
		message, err := d.DG.ChannelMessageSendEmbed(channelID, embed)

		if err != nil {
			var discErr DiscordErr
			umErr := json.Unmarshal([]byte(err.Error()), &discErr)

			if umErr != nil {
				return nil, &utils.ServiceError{
					StatusCode: 0,
					Err:        err,
					Message:    "Failed to unmarshal create embed post error",
				}
			}

			return nil, &utils.ServiceError{
				StatusCode: discErr.Code,
				Err:        err,
				Message:    discErr.Message,
			}
		}

		return message, nil
	}

	if content != "" {
		message, err := d.DG.ChannelMessageSend(channelID, content)

		if err != nil {
			var discErr DiscordErr
			umErr := json.Unmarshal([]byte(err.Error()), &discErr)

			if umErr != nil {
				return nil, &utils.ServiceError{
					StatusCode: 0,
					Err:        err,
					Message:    "Failed to unmarshal create post error",
				}
			}

			return nil, &utils.ServiceError{
				StatusCode: discErr.Code,
				Err:        err,
				Message:    discErr.Message,
			}
		}

		return message, nil
	}

	return nil, &utils.ServiceError{
		StatusCode: 0,
		Err:        fmt.Errorf("No content or embed set to create post"),
		Message:    "No content or embed set to create post",
	}
}

// CreateReaction func
func (d *Discord) CreateReaction(channelID string, messageID string, emojiID string) *utils.ServiceError {
	err := d.DG.MessageReactionAdd(channelID, messageID, emojiID)

	if err != nil {
		var discErr DiscordErr
		umErr := json.Unmarshal([]byte(err.Error()), &discErr)

		if umErr != nil {
			return &utils.ServiceError{
				StatusCode: 0,
				Err:        err,
				Message:    "Failed to add reaction to post",
			}
		}

		return &utils.ServiceError{
			StatusCode: discErr.Code,
			Err:        err,
			Message:    discErr.Message,
		}
	}

	return nil
}
