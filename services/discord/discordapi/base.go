package discordapi

import (
	"encoding/json"
	"fmt"
	"net/http"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// Discord struct
type Discord struct {
	DG  *discordgo.Session
	Log *utils.Log
}

// DiscordErr struct
type DiscordErr struct {
	Message string `json:"message"`
	Code    int    `json:"code"`
}

// ParseError func
func ParseError(err error) (*utils.ServiceError, *utils.ServiceError) {
	splitErr := strings.SplitN(err.Error(), ", ", 2)

	if len(splitErr) != 2 {
		return nil, &utils.ServiceError{
			StatusCode: http.StatusBadRequest,
			Err:        fmt.Errorf(err.Error()),
			Message:    "Invalid format for Discord error",
		}
	}

	var discErr DiscordErr
	umErr := json.Unmarshal([]byte(splitErr[1]), &discErr)

	if umErr != nil {
		return nil, &utils.ServiceError{
			StatusCode: http.StatusBadRequest,
			Err:        umErr,
			Message:    "Unable to unmarshal Discord error",
		}
	}

	return &utils.ServiceError{
		StatusCode: discErr.Code,
		Err:        err,
		Message:    discErr.Message,
	}, nil
}
