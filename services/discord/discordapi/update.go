package discordapi

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// UpdatePost func
func (d *Discord) UpdatePost(channelID string, messageID string, content string, embed *discordgo.MessageEmbed) (*discordgo.Message, *utils.ServiceError) {
	if embed != nil {
		message, err := d.DG.ChannelMessageEditEmbed(channelID, messageID, embed)

		if err != nil {
			discErr, parseErr := ParseError(err)

			if parseErr != nil {
				return nil, parseErr
			}

			return nil, discErr
		}

		return message, nil
	}

	if content != "" {
		message, err := d.DG.ChannelMessageEdit(channelID, messageID, content)

		if err != nil {
			discErr, parseErr := ParseError(err)

			if parseErr != nil {
				return nil, parseErr
			}

			return nil, discErr
		}

		return message, nil
	}

	return nil, &utils.ServiceError{
		StatusCode: 0,
		Err:        fmt.Errorf("No content or embed set to update post"),
		Message:    "No content or embed set to update post",
	}
}
