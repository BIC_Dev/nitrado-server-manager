package commands

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// Banlist struct
type Banlist struct {
	Players   []nitrado.Player
	Gamserver filestore.Gameserver
}

// BanlistError struct
type BanlistError struct {
	Errs        []*utils.ServiceError
	Gameservers []filestore.Gameserver
}

// MaxBanlistEmbedLength limits size of embed to not hit 6000 character limit
const MaxBanlistEmbedLength = 8

// BanlistCommand func
func (ch *CommandHandler) BanlistCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: List Bans: %s", message.Content), ch.Log.LogInformation)

	var singleBanListGameserver *filestore.Gameserver

	if len(args) > 0 {
		potentialGameserverID, pe := strconv.Atoi(args[0])

		if pe == nil {
			for _, gameserver := range ch.Guild.Gameservers {
				if gameserver.ID == potentialGameserverID {
					singleBanListGameserver = &gameserver
					break
				}
			}
		}

		if singleBanListGameserver == nil {
			ch.SendErrorMessage("ERROR: Banlist", "Invalid gameserver ID", fmt.Sprintf("Please use the `%s%s` command to get the gameserver ID", ch.Guild.Bot.Prefix, ch.Config.Bot.Commands["servers"].Name), "", message.Content, message.ChannelID)
			return
		}
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":      "false",
		"exact-match": "false",
	}

	if singleBanListGameserver != nil {
		requestData = append(requestData, nitrado.RequestData{
			Command:    command,
			Gameserver: *singleBanListGameserver,
		})
	} else {
		for _, gameserver := range ch.Guild.Gameservers {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Gameserver: gameserver,
			})
		}
	}

	responses, errors := request.GoRequest(request.GetBanlist, requestData, params)

	var embeddableBanlist []EmbeddableField

	for _, banlist := range responses {
		playerLen := len(banlist.Players)
		sliceStart := 0
		embedFieldSize := 0

		for i := 0; i < playerLen; i++ {
			embedFieldSize += len(banlist.Players[i].Name)
			if embedFieldSize >= 500 || i+1 >= playerLen {
				embeddableBanlist = append(embeddableBanlist, &Banlist{
					Players:   banlist.Players[sliceStart : i+1],
					Gamserver: banlist.Gameserver,
				})
				sliceStart = i + 1
				embedFieldSize = 0
			}
		}
	}

	var ble BanlistError

	for _, err := range errors {
		ble.Errs = append(ble.Errs, err.Error)
		ble.Gameservers = append(ble.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(ble.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ble)
	}

	var description string = fmt.Sprintf("**Command:** `%s`", message.Content)

	embedParams := EmbeddableParams{
		Title:       "Get Banlist",
		Description: description,
		Color:       ch.Guild.Bot.Commands["player"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	if len(embeddableBanlist) == 0 {
		embed := CreateEmbed(embedParams, embeddableBanlist, embeddableErrors)

		// Can get message ID from response here
		_, cpErr := discord.CreatePost(message.ChannelID, "", embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post empty banlist: %s", cpErr.Error()), ch.Log.LogMedium)
		}

		return
	}

	banlistSliceLen := len(embeddableBanlist)
	banlistSliceChunks := float64(banlistSliceLen) / float64(MaxBanlistEmbedLength)

	for i := 0; float64(i) < banlistSliceChunks; i++ {
		start := i * MaxBanlistEmbedLength
		end := start + MaxBanlistEmbedLength

		if end > banlistSliceLen {
			end = banlistSliceLen
		}

		embed := CreateEmbed(embedParams, embeddableBanlist[start:end], embeddableErrors)

		// Can get message ID from response here
		_, cpErr := discord.CreatePost(message.ChannelID, "", embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post banlist: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}

	return
}

// ConvertToEmbedField func
func (bl *Banlist) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var fieldVal string

	for _, player := range bl.Players {
		if player.Name == "" {
			continue
		}

		if fieldVal == "" {
			fieldVal = player.Name
		} else {
			fieldVal = fmt.Sprintf("%s\n%s", fieldVal, player.Name)
		}
	}

	var field *discordgo.MessageEmbedField

	field = &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("Banned Players on %s", bl.Gamserver.Name),
		Value:  fmt.Sprintf("```\n%s\n```", fieldVal),
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (ble *BanlistError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range ble.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to retreive banlist from gameservers",
		Value:  fmt.Sprintf("```%s```", strings.Join(gameservers, "\n")),
		Inline: false,
	}

	return field, nil
}
