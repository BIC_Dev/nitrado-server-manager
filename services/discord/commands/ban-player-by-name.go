package commands

import (
	"fmt"
	"net/http"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// BanPlayer struct
type BanPlayer struct {
	PlayerName string
	PlayerID   string
	Servers    []string
}

// BanPlayerError struct
type BanPlayerError struct {
	Errs        []*utils.ServiceError
	Gameservers []filestore.Gameserver
}

const minLengthBanPlayerName = 3

// BanPlayerByNameCommand func
func (ch *CommandHandler) BanPlayerByNameCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Ban player: %s", message.Content), ch.Log.LogInformation)

	if len(args) < 1 {
		ch.SendErrorMessage("ERROR: Ban Player", "Invalid playername", fmt.Sprintf("Must include %d or more characters", minLengthSearchPlayerName), "", message.Content, message.ChannelID)
		return
	}

	var singleBanGameserver *filestore.Gameserver
	potentialGameserverID, pe := strconv.Atoi(args[0])

	if pe == nil {
		for _, gameserver := range ch.Guild.Gameservers {
			if gameserver.ID == potentialGameserverID {
				singleBanGameserver = &gameserver
				break
			}
		}
	}

	var playerName string

	if singleBanGameserver != nil {
		if len(args) < 2 {
			ch.SendErrorMessage("ERROR: Ban Player", "Missing player name", fmt.Sprintf("Must include player name with %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
			return
		}

		playerName = strings.Join(args[1:], " ")
	} else if len(args[0]) > 5 && pe == nil {
		ch.SendErrorMessage("ERROR: Ban Player", "Invalid gameserver ID", fmt.Sprintf("Please use the `%s%s` command to get the gameserver ID", ch.Guild.Bot.Prefix, ch.Config.Bot.Commands["servers"].Name), "", message.Content, message.ChannelID)
		return
	} else {
		playerName = strings.Join(args, " ")
	}

	if len(playerName) < minLengthBanPlayerName {
		ch.SendErrorMessage("ERROR: Ban Player", "Invalid playername", fmt.Sprintf("Must include %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData
	params := map[string]string{
		"online":      "false",
		"exact-match": "true",
	}

	for _, gameserver := range ch.Guild.Gameservers {
		requestData = append(requestData, nitrado.RequestData{
			Command:    command,
			Player:     nitrado.Player{Name: playerName},
			Gameserver: gameserver,
		})
	}

	responses, errors := request.GoRequest(request.SearchPlayersByName, requestData, params)

	// if len(responses) == 0 {
	// 	ch.SendErrorMessage("Ban Player", "No players found", "", "", message.Content, message.ChannelID)
	// 	return
	// }

	sp := ParsePlayersFromRequestResponse(responses)

	var embeddablePlayers []EmbeddableField

	for _, player := range sp {
		embeddablePlayers = append(embeddablePlayers, player)
	}

	var spe SearchPlayerError

	for _, err := range errors {
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		spe.Errs = append(spe.Errs, err.Error)
		spe.Gameservers = append(spe.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(spe.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &spe)
	}

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddablePlayers) > 0 {
		if singleBanGameserver != nil {
			description += fmt.Sprintf("\nClick on the %s reaction to **ban** the player on %s", ch.Command.Reactions["ban_player"].Icon, singleBanGameserver.Name)
		} else {
			description += fmt.Sprintf("\nClick on the %s reaction to **ban** the player on all servers", ch.Command.Reactions["ban_player"].Icon)
		}
	} else {
		description += "\n**No player found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Ban Player by Name",
		Description: description,
		Color:       ch.Guild.Bot.Commands["ban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddablePlayers, embeddableErrors)

	// Can get message ID from response here
	postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post ban player: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	if len(embeddablePlayers) == 0 {
		return
	}

	prErr := ch.PostReaction(postedMessage.ChannelID, postedMessage.ID, ch.Command.Reactions["ban_player"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	var mErr *utils.ModelError

	if singleBanGameserver != nil {
		mErr = AddPotentialBanToDB(ch.Config, postedMessage.ID, message.Author.ID, sp, singleBanGameserver.ID)
	} else {
		mErr = AddPotentialBanToDB(ch.Config, postedMessage.ID, message.Author.ID, sp, 0)
	}

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// HandleBanReaction func
func (ch *CommandHandler) HandleBanReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	ban, gErr := GetPotentialBanFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if ban.PlayerID == "" {
		ch.Log.Log("ERROR: No potential ban found", ch.Log.LogInformation)
		return
	}

	if ban.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to ban user", ch.Log.LogInformation)
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	for _, gameserver := range ch.Guild.Gameservers {
		if ban.GameserverID == 0 {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: ban.PlayerID},
				Gameserver: gameserver,
			})
		} else if ban.GameserverID == gameserver.ID {
			requestData = append(requestData, nitrado.RequestData{
				Command:    command,
				Player:     nitrado.Player{ID: ban.PlayerID},
				Gameserver: gameserver,
			})
		}
	}

	responses, errors := request.GoRequest(request.BanPlayer, requestData, nil)

	b := ParseGameserversFromBanRequestResponse(responses)

	var embeddableFields []EmbeddableField

	if b != nil {
		b.PlayerID = ban.PlayerID
		b.PlayerName = ban.PlayerName
		embeddableFields = append(embeddableFields, b)
	}

	var ube UnbanPlayerError

	for _, err := range errors {
		// Ignore 404 errors
		if err.Error.StatusCode == http.StatusNotFound {
			continue
		}

		ube.Errs = append(ube.Errs, err.Error)
		ube.Gameservers = append(ube.Gameservers, err.Gameserver)
	}

	var embeddableErrors []EmbeddableError

	if len(ube.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &ube)
	}

	embedParams := EmbeddableParams{
		Title:       "Ban Player by Name",
		Description: fmt.Sprintf("**Banned:** `%s`\nThis will be applied when Nitrado updates Ark with the new ban. It may take some time.", b.PlayerName),
		Color:       ch.Guild.Bot.Commands["ban"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	// Can get message ID from response here
	_, cpErr := discord.CreatePost(reaction.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post ban player message: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	dErr := DeletePotentialBanFromDB(ch.Config, ban)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// ConvertToEmbedField func
func (sp *BanPlayer) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var servers string

	for _, server := range sp.Servers {
		servers = servers + "\n\t" + server
	}

	fieldVal := fmt.Sprintf("```Player ID: %s\nServers: %s```", sp.PlayerID, servers)

	field := &discordgo.MessageEmbedField{
		Name:   sp.PlayerName,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *BanPlayerError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range spe.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to ban player on gameservers",
		Value:  fmt.Sprintf("```%s```", strings.Join(gameservers, "\n")),
		Inline: false,
	}

	return field, nil
}

// ParseGameserversFromBanRequestResponse func
func ParseGameserversFromBanRequestResponse(rr []*nitrado.RequestResponse) *BanPlayer {
	var ubp BanPlayer

	for _, gs := range rr {
		ubp.Servers = append(ubp.Servers, gs.Gameserver.Name)
	}

	return &ubp
}

// AddPotentialBanToDB func
func AddPotentialBanToDB(config *utils.Config, messageID string, userID string, players map[string]*SearchPlayer, gameserverID int) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	for _, player := range players {
		potentialBan := &models.PotentialBan{
			MessageID:     messageID,
			PlayerID:      player.ID,
			PlayerName:    player.Name,
			DiscordUserID: userID,
			GameserverID:  gameserverID,
		}

		mErr := potentialBan.Create(dbInt)

		if mErr != nil {
			return mErr
		}
	}

	return nil
}

// GetPotentialBanFromDB func
func GetPotentialBanFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialBan, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialBan := &models.PotentialBan{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbBan, mErr := potentialBan.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbBan, nil
}

// DeletePotentialBanFromDB func
func DeletePotentialBanFromDB(config *utils.Config, ban *models.PotentialBan) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := ban.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
