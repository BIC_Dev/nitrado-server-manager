package commands

import (
	"fmt"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// HelpCommandInfo struct
type HelpCommandInfo struct {
	Prefix       string
	Command      utils.Command
	GuildCommand filestore.Command
}

// HelpRunnerInfo struct
type HelpRunnerInfo struct {
	Name        string
	Interval    int
	Description string
}

// HelpCommand func
func (ch *CommandHandler) HelpCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Help: %s", message.Content), ch.Log.LogInformation)

	var embeddableInfo []EmbeddableField
	var embeddableErrors []EmbeddableError

	for _, val := range ch.Config.Bot.Commands {
		if cmd, ok := ch.Guild.Bot.Commands[val.Name]; ok {
			embeddableInfo = append(embeddableInfo, &HelpCommandInfo{
				Prefix:       ch.Guild.Bot.Prefix,
				Command:      val,
				GuildCommand: cmd,
			})
		}
	}

	embeddableInfo = append(embeddableInfo, &HelpRunnerInfo{
		Name:        "Online Players List",
		Interval:    ch.Config.Runners.PlayersInterval,
		Description: "A per-server listing of currently online players. This will automatically post to one Discord channel or separated by gameserver into multiple Discord channels.",
	})

	embeddableInfo = append(embeddableInfo, &HelpRunnerInfo{
		Name:        "Global Chat Log",
		Interval:    ch.Config.Runners.LogsInterval,
		Description: "A per-server chat log. You will see new chat entries whenever Nitrado writes to their log files. This will automatically post to one Discord channel or separated by gameserver into multiple Discord channels.",
	})

	embeddableInfo = append(embeddableInfo, &HelpRunnerInfo{
		Name:        "Admin Command Log",
		Interval:    ch.Config.Runners.LogsInterval,
		Description: "A per-server admin command log. You will see new chat entries whenever Nitrado writes to their log files. This will automatically post to one Discord channel or separated by gameserver into multiple Discord channels.",
	})

	embedParams := EmbeddableParams{
		Title:       "Help Information",
		Description: "Help information for the Nitrado Server Manager",
		Color:       ch.Guild.Bot.Commands["help"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	embed := CreateEmbed(embedParams, embeddableInfo, embeddableErrors)

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	_, cpErr := discord.CreatePost(message.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post help information: %s", cpErr.Error()), ch.Log.LogMedium)
	}
}

// ConvertToEmbedField func
func (hci *HelpCommandInfo) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var roles string = ""

	for _, role := range hci.GuildCommand.Roles {
		if roles == "" {
			roles = fmt.Sprintf("<@&%d>", role)
		} else {
			roles += fmt.Sprintf(", <@&%d>", role)
		}
	}

	if roles == "" {
		roles = "@everyone"
	}

	var examples string

	for _, ex := range hci.Command.Examples {
		if examples == "" {
			examples = fmt.Sprintf("**Example:** `%s%s`", hci.Prefix, ex)
		} else {
			examples += fmt.Sprintf("\n**Example:** `%s%s`", hci.Prefix, ex)
		}
	}

	field := &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("__%s__", strings.Title(hci.Command.Name)),
		Value:  fmt.Sprintf("%s\n**Description:** %s\n**Roles:** %s\n\u200b", examples, hci.Command.Description, roles),
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (hci *HelpRunnerInfo) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("__%s__", strings.Title(hci.Name)),
		Value:  fmt.Sprintf("**Interval:** Refreshes every %d seconds\n**Description:** %s\n\u200b", hci.Interval, hci.Description),
		Inline: false,
	}

	return field, nil
}
