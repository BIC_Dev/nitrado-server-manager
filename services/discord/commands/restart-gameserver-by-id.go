package commands

import (
	"fmt"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// RestartGameserver struct
type RestartGameserver struct {
	Gameserver *filestore.Gameserver
}

// RestartGameserverError struct
type RestartGameserverError struct {
	Errs        []*utils.ServiceError
	Gameservers []filestore.Gameserver
}

const minLengthRestartGameserverName = 7

// RestartGameserverCommand func
func (ch *CommandHandler) RestartGameserverCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Restart Gameserver: %s", message.Content), ch.Log.LogInformation)

	if len(args) == 0 || len(args) > 1 {
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Invalid gameserver ID", fmt.Sprintf("Must include %d or more characters", minLengthRestartGameserverName), "", message.Content, message.ChannelID)
		return
	}

	gameserver, ggErr := ch.GetGameserverByID(args[0])

	var embeddableGameserver []EmbeddableField

	if gameserver != nil {
		embeddableGameserver = append(embeddableGameserver, gameserver)
	}

	var embeddableErrors []EmbeddableError

	if ggErr != nil {
		embeddableErrors = append(embeddableErrors, &RestartGameserverError{
			Errs: []*utils.ServiceError{ggErr},
		})
	}

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddableGameserver) > 0 {
		description += fmt.Sprintf("\nClick on the %s reaction to **restart** the gameserver", ch.Command.Reactions["restart_gameserver"].Icon)
	} else {
		description += "\n**No gameserver found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Restart Gameserver by ID",
		Description: description,
		Color:       ch.Guild.Bot.Commands["restart"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddableGameserver, embeddableErrors)

	// Can get message ID from response here
	postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post restart gameserver: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	if len(embeddableGameserver) == 0 {
		return
	}

	prErr := ch.PostReaction(postedMessage.ChannelID, postedMessage.ID, ch.Command.Reactions["restart_gameserver"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	mErr := AddPotentialRestartGameserverToDB(ch.Config, postedMessage.ID, message.Author.ID, gameserver.Gameserver)

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// ConvertToEmbedField func
func (sg *RestartGameserver) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	fieldVal := fmt.Sprintf("```ID: %d```", sg.Gameserver.ID)

	field := &discordgo.MessageEmbedField{
		Name:   sg.Gameserver.Name,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *RestartGameserverError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to find gameserver by ID",
		Value:  "\u200B",
		Inline: false,
	}

	return field, nil
}

// HandleRestartGameserverReaction func
func (ch *CommandHandler) HandleRestartGameserverReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	sg, gErr := GetPotentialRestartGameserverFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if sg.GameserverID == 0 {
		ch.Log.Log("ERROR: No potential gameserver found", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Unable to find original request to restart gameserver", "Please try to restart the gameserver again. It should fix the issue.", "", "", reaction.ChannelID)
		return
	}

	if sg.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to restart gameserver", ch.Log.LogInformation)
		return
	}

	gameserver, gsErr := ch.GetGameserverByID(fmt.Sprintf("%d", sg.GameserverID))

	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Gameserver does not exist for ID: %d", sg.GameserverID), ch.Log.LogMedium)
		ch.SendErrorMessage("ERROR: Restart Gameserver", "Gameserver ID does not exist", "This could happen if you have changed your gameservers since opening the restart gameserver ticket.", "", fmt.Sprintf("stop %d", sg.GameserverID), reaction.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	requestData = append(requestData, nitrado.RequestData{
		Command:    command,
		Gameserver: *gameserver.Gameserver,
	})

	responses, errors := request.GoRequest(request.RestartGameserver, requestData, nil)

	var embeddableFields []EmbeddableField
	var embeddableErrors []EmbeddableError

	if responses != nil && len(responses) > 0 {
		embeddableFields = append(embeddableFields, &RestartGameserver{
			Gameserver: &responses[0].Gameserver,
		})
	}

	if errors != nil && len(errors) > 0 {
		embeddableErrors = append(embeddableErrors, &RestartGameserverError{
			Errs: []*utils.ServiceError{
				errors[0].Error,
			},
			Gameservers: []filestore.Gameserver{
				*gameserver.Gameserver,
			},
		})
	}

	embedParams := EmbeddableParams{
		Title:       "Restart Gameserver by ID",
		Description: fmt.Sprintf("**Restarted:** `%s`", gameserver.Gameserver.Name),
		Color:       ch.Guild.Bot.Commands["stop"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	// Can get message ID from response here
	_, cpErr := discord.CreatePost(reaction.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post restart gameserver message: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	dErr := DeletePotentialRestartGameserverFromDB(ch.Config, sg)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// AddPotentialRestartGameserverToDB func
func AddPotentialRestartGameserverToDB(config *utils.Config, messageID string, userID string, gameserver *filestore.Gameserver) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	potentialRestart := &models.PotentialRestartGameserver{
		MessageID:      messageID,
		GameserverID:   gameserver.ID,
		GameserverName: gameserver.Name,
		DiscordUserID:  userID,
	}

	mErr := potentialRestart.Create(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}

// GetPotentialRestartGameserverFromDB func
func GetPotentialRestartGameserverFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialRestartGameserver, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialRestartGameserver := &models.PotentialRestartGameserver{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbRes, mErr := potentialRestartGameserver.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbRes, nil
}

// DeletePotentialRestartGameserverFromDB func
func DeletePotentialRestartGameserverFromDB(config *utils.Config, sg *models.PotentialRestartGameserver) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := sg.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
