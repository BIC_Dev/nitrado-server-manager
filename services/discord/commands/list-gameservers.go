package commands

import (
	"fmt"
	"sort"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// ListGameserversCommand func
func (ch *CommandHandler) ListGameserversCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: List gameservers: %s", message.Content), ch.Log.LogInformation)

	var color int = command.Color

	if val, ok := ch.Guild.Bot.Commands[command.Name]; ok {
		if val.Color != 0 {
			color = val.Color
		}
	}

	embeds := formatListGameserversEmbed(ch.Config, ch.Guild, command.Name, color)

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	for _, embed := range embeds {
		_, cpErr := discord.CreatePost(message.ChannelID, "", &embed)

		if cpErr != nil {
			ch.Log.Log(fmt.Sprintf("ERROR: Failed to post embed to discord for list gameservers: %s", cpErr.Error()), ch.Log.LogMedium)
		}
	}
}

func formatListGameserversEmbed(c *utils.Config, g *filestore.Guild, command string, embedColor int) []discordgo.MessageEmbed {
	var embeds []discordgo.MessageEmbed

	embed := discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Listed",
		},
		Color:       embedColor,
		Description: fmt.Sprintf("**Command:** `%s`", command),
		Fields:      []*discordgo.MessageEmbedField{},
		Timestamp:   time.Now().Format(time.RFC3339), // Discord wants ISO8601; RFC3339 is an extension of ISO8601 and should be completely compatible.
		Title:       "Server List",
		URL:         c.Bot.TitleURL,
	}

	if len(g.Gameservers) == 0 {
		field := &discordgo.MessageEmbedField{
			Name:   "No Gameservers",
			Value:  "Please contact BIC to add gameservers to your config",
			Inline: false,
		}

		newEmbed := embed

		newEmbed.Fields = append(newEmbed.Fields, field)

		embeds = append(embeds, newEmbed)
	} else {
		gameservers := g.Gameservers

		sort.SliceStable(gameservers, func(i, j int) bool {
			return strings.ToLower(gameservers[i].Name) < strings.ToLower(gameservers[j].Name)
		})

		embedNum := 0
		embedFields := 1

		for _, gameserver := range gameservers {
			if embeds == nil || len(embeds) == 0 {
				newEmbed := embed
				embeds = append(embeds, newEmbed)
			}

			if embedFields >= 21 {
				embedNum++
				embedFields = 1
				newEmbed := embed
				embeds = append(embeds, newEmbed)
			}

			field := &discordgo.MessageEmbedField{
				Name:   gameserver.Name,
				Value:  fmt.Sprintf("ID: %d", gameserver.ID),
				Inline: false,
			}

			embeds[embedNum].Fields = append(embeds[embedNum].Fields, field)
			embedFields++
		}
	}

	return embeds
}
