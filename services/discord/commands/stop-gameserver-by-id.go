package commands

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/models"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// StopGameserver struct
type StopGameserver struct {
	Gameserver *filestore.Gameserver
}

// StopGameserverError struct
type StopGameserverError struct {
	Errs        []*utils.ServiceError
	Gameservers []filestore.Gameserver
}

const minLengthStopGameserverName = 7

// StopGameserverCommand func
func (ch *CommandHandler) StopGameserverCommand(command utils.Command, message *discordgo.MessageCreate, args []string) {
	ch.Log.Log(fmt.Sprintf("COMMAND: Stop gameserver: %s", message.Content), ch.Log.LogInformation)

	if len(args) == 0 || len(args) > 1 {
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Invalid gameserver ID", fmt.Sprintf("Must include %d or more characters", minLengthBanPlayerName), "", message.Content, message.ChannelID)
		return
	}

	gameserver, ggErr := ch.GetGameserverByID(args[0])

	var embeddableGameserver []EmbeddableField

	if gameserver != nil {
		embeddableGameserver = append(embeddableGameserver, gameserver)
	}

	var embeddableErrors []EmbeddableError

	if ggErr != nil {
		embeddableErrors = append(embeddableErrors, &StopGameserverError{
			Errs: []*utils.ServiceError{ggErr},
		})
	}

	description := fmt.Sprintf("**Command:** `%s`", message.Content)

	if len(embeddableGameserver) > 0 {
		description += fmt.Sprintf("\nClick on the %s reaction to **stop** the gameserver", ch.Command.Reactions["stop_gameserver"].Icon)
	} else {
		description += "\n**No gameserver found**"
	}

	embedParams := EmbeddableParams{
		Title:       "Stop Gameserver by ID",
		Description: description,
		Color:       ch.Guild.Bot.Commands["stop"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddableGameserver, embeddableErrors)

	// Can get message ID from response here
	postedMessage, cpErr := discord.CreatePost(message.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post stop gameserver: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	if len(embeddableGameserver) == 0 {
		return
	}

	prErr := ch.PostReaction(postedMessage.ChannelID, postedMessage.ID, ch.Command.Reactions["stop_gameserver"].Icon)

	if prErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", prErr.GetMessage(), prErr.Error()), ch.Log.LogMedium)
		return
	}

	mErr := AddPotentialStopGameserverToDB(ch.Config, postedMessage.ID, message.Author.ID, gameserver.Gameserver)

	if mErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", mErr.GetMessage(), mErr.Error()), ch.Log.LogMedium)
	}
}

// GetGameserverByID func
func (ch *CommandHandler) GetGameserverByID(gameserverIDString string) (*StopGameserver, *utils.ServiceError) {
	gameserverID, cErr := strconv.Atoi(gameserverIDString)

	if cErr != nil {
		return nil, &utils.ServiceError{
			StatusCode: http.StatusBadRequest,
			Err:        cErr,
			Message:    fmt.Sprintf("Failed to convert gameserver ID to int: %s", gameserverIDString),
		}
	}

	for _, gameserver := range ch.Guild.Gameservers {
		if gameserver.ID == gameserverID {
			return &StopGameserver{
				Gameserver: &gameserver,
			}, nil
		}
	}

	return nil, &utils.ServiceError{
		StatusCode: http.StatusBadRequest,
		Err:        fmt.Errorf("No such gameserver in guild config"),
		Message:    fmt.Sprintf("Unable to find gameserver ID: %s", gameserverIDString),
	}
}

// ConvertToEmbedField func
func (sg *StopGameserver) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	fieldVal := fmt.Sprintf("```ID: %d```", sg.Gameserver.ID)

	field := &discordgo.MessageEmbedField{
		Name:   sg.Gameserver.Name,
		Value:  fieldVal,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (spe *StopGameserverError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed to find gameserver by ID",
		Value:  "\u200B",
		Inline: false,
	}

	return field, nil
}

// HandleStopGameserverReaction func
func (ch *CommandHandler) HandleStopGameserverReaction(command utils.Command, reaction *discordgo.MessageReactionAdd) {
	sg, gErr := GetPotentialStopGameserverFromDB(ch.Config, reaction.MessageID, reaction.UserID)

	if gErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", gErr.GetMessage(), gErr.Error()), ch.Log.LogMedium)
		return
	}

	if sg.GameserverID == 0 {
		ch.Log.Log("ERROR: No potential gameserver found", ch.Log.LogInformation)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Unable to find original request to stop gameserver", "Please try to stop the gameserver again. It should fix the issue.", "", "", reaction.ChannelID)
		return
	}

	if sg.DiscordUserID != reaction.UserID {
		ch.Log.Log("ERROR: Wrong discord user tried to stop gameserver", ch.Log.LogInformation)
		return
	}

	gameserver, gsErr := ch.GetGameserverByID(fmt.Sprintf("%d", sg.GameserverID))

	if gsErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Gameserver does not exist for ID: %d", sg.GameserverID), ch.Log.LogMedium)
		ch.SendErrorMessage("ERROR: Stop Gameserver", "Gameserver ID does not exist", "This could happen if you have changed your gameservers since opening the stop gameserver ticket.", "", fmt.Sprintf("stop %d", sg.GameserverID), reaction.ChannelID)
		return
	}

	request := nitrado.NitradoRequest{
		Config:                ch.Config,
		NitradoV2ServiceToken: ch.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	requestData = append(requestData, nitrado.RequestData{
		Command:    command,
		Gameserver: *gameserver.Gameserver,
	})

	responses, errors := request.GoRequest(request.StopGameserver, requestData, nil)

	var embeddableFields []EmbeddableField
	var embeddableErrors []EmbeddableError

	if responses != nil && len(responses) > 0 {
		embeddableFields = append(embeddableFields, &StopGameserver{
			Gameserver: &responses[0].Gameserver,
		})
	}

	if errors != nil && len(errors) > 0 {
		embeddableErrors = append(embeddableErrors, &StopGameserverError{
			Errs: []*utils.ServiceError{
				errors[0].Error,
			},
			Gameservers: []filestore.Gameserver{
				*gameserver.Gameserver,
			},
		})
	}

	embedParams := EmbeddableParams{
		Title:       "Stop Gameserver by ID",
		Description: fmt.Sprintf("**Stopped:** `%s`", gameserver.Gameserver.Name),
		Color:       ch.Guild.Bot.Commands["stop"].Color,
		TitleURL:    ch.Config.Bot.TitleURL,
	}

	discord := discordapi.Discord{
		DG:  ch.DG,
		Log: ch.Log,
	}

	embed := CreateEmbed(embedParams, embeddableFields, embeddableErrors)

	// Can get message ID from response here
	_, cpErr := discord.CreatePost(reaction.ChannelID, "", embed)

	if cpErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: Failed to post stop gameserver message: %s", cpErr.Error()), ch.Log.LogMedium)
	}

	dErr := DeletePotentialStopGameserverFromDB(ch.Config, sg)

	if dErr != nil {
		ch.Log.Log(fmt.Sprintf("ERROR: %s: %s", dErr.GetMessage(), dErr.Error()), ch.Log.LogMedium)
	}
}

// AddPotentialStopGameserverToDB func
func AddPotentialStopGameserverToDB(config *utils.Config, messageID string, userID string, gameserver *filestore.Gameserver) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return dbErr
	}

	defer dbInt.GetDB().Close()

	potentialBan := &models.PotentialStopGameserver{
		MessageID:      messageID,
		GameserverID:   gameserver.ID,
		GameserverName: gameserver.Name,
		DiscordUserID:  userID,
	}

	mErr := potentialBan.Create(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}

// GetPotentialStopGameserverFromDB func
func GetPotentialStopGameserverFromDB(config *utils.Config, messageID string, userID string) (*models.PotentialStopGameserver, *utils.ModelError) {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil, dbErr
	}

	defer dbInt.GetDB().Close()

	potentialStopGameserver := &models.PotentialStopGameserver{
		MessageID:     messageID,
		DiscordUserID: userID,
	}

	dbRes, mErr := potentialStopGameserver.GetLatest(dbInt)

	if mErr != nil {
		return nil, mErr
	}

	return &dbRes, nil
}

// DeletePotentialStopGameserverFromDB func
func DeletePotentialStopGameserverFromDB(config *utils.Config, sg *models.PotentialStopGameserver) *utils.ModelError {
	dbInt, dbErr := db.GetDB(config)

	if dbErr != nil {
		return nil
	}

	defer dbInt.GetDB().Close()

	mErr := sg.Delete(dbInt)

	if mErr != nil {
		return mErr
	}

	return nil
}
