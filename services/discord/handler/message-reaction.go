package handler

import (
	"fmt"
	"net/http"
	"strconv"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// MessageReaction func
func (dh *DiscordHandler) MessageReaction(s *discordgo.Session, m *discordgo.MessageReactionAdd) {
	if m.UserID == s.State.User.ID {
		return
	}

	guildID, pErr := strconv.ParseInt(m.GuildID, 10, 64)

	if pErr != nil {
		dh.Log.Log("ERROR: Could not convert guildID string to int64", dh.Log.LogLow)
		// Consider sending Discord message on failure
		return
	}

	var guild *filestore.Guild

	if val, ok := dh.GuildConfig.Guilds[guildID]; ok {
		guild = &val
	} else {
		dh.Log.Log(fmt.Sprintf("ERROR: Guild not in config with ID: %d", guildID), dh.Log.LogLow)
		return
	}

	command, cErr := GetReactionCommand(dh.Config, m.Emoji.Name)

	if cErr != nil {
		return
	}

	commandHandler := &commands.CommandHandler{
		Config:                dh.Config,
		Log:                   dh.Log,
		DG:                    dh.DG,
		NitradoV2ServiceToken: dh.NitradoV2ServiceToken,
		Guild:                 guild,
		Command:               command,
		Reaction:              command.Reactions[m.Emoji.ID],
	}

	guildMember, gmErr := dh.DG.GuildMember(m.GuildID, m.UserID)

	if gmErr != nil {
		dh.Log.Log(fmt.Sprintf("ERROR: Could not look up guild member to validate roles: %s", gmErr.Error()), dh.Log.LogMedium)
		return
	}

	var foundRole = false

	if val, ok := guild.Bot.Commands[command.Name]; ok {
		if len(val.Roles) == 0 {
			foundRole = true
		}

		for _, commandRole := range val.Roles {
			commandRoleString := strconv.FormatInt(commandRole, 10)
			for _, userRole := range guildMember.Roles {
				if commandRoleString == userRole {
					foundRole = true
					break
				}
			}

			if foundRole {
				break
			}
		}
	}

	if !foundRole {
		dh.Log.Log(fmt.Sprintf("ERROR: Insufficient permissions to use reactions for: %s", command.Name), dh.Log.LogLow)
		return
	}

	switch command.Name {
	case "ban":
		commandHandler.HandleBanReaction(command, m)
	case "unban":
		commandHandler.HandleUnbanReaction(command, m)
	case "stop":
		commandHandler.HandleStopGameserverReaction(command, m)
	case "restart":
		commandHandler.HandleRestartGameserverReaction(command, m)
	default:
		dh.Log.Log(fmt.Sprintf("Reaction for command not implemented: %s\n", command.Name), dh.Log.LogInformation)
	}
}

// GetReactionCommand func
func GetReactionCommand(config *utils.Config, reactionName string) (utils.Command, *utils.ServiceError) {
	for _, aCommand := range config.Bot.Commands {
		for _, aReaction := range aCommand.Reactions {
			if reactionName == aReaction.Icon {
				return aCommand, nil
			}
		}
	}

	sErr := utils.NewServiceError(fmt.Errorf("No command associated with reaction"))
	sErr.SetMessage(fmt.Sprintf("No command associated with reaction name: %s", reactionName))
	sErr.SetStatus(http.StatusNotFound)

	return utils.Command{}, sErr
}
