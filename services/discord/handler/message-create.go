package handler

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// MessageCreate func
func (dh *DiscordHandler) MessageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {
	if m.Author.ID == s.State.User.ID {
		return
	}

	if m.Author.Bot {
		return
	}

	guildID, pErr := strconv.ParseInt(m.GuildID, 10, 64)

	if pErr != nil {
		dh.Log.Log("ERROR: Could not convert guildID string to int64", dh.Log.LogLow)
		// Consider sending Discord message on failure
		return
	}

	var guild *filestore.Guild

	if val, ok := dh.GuildConfig.Guilds[guildID]; ok {
		guild = &val
	} else {
		dh.Log.Log(fmt.Sprintf("ERROR: Guild not in config with ID: %d", guildID), dh.Log.LogLow)
		return
	}

	if guild.Bot.Prefix != getPrefix(m.Content, len(guild.Bot.Prefix)) {
		return
	}

	commandHandler := &commands.CommandHandler{
		Config:                dh.Config,
		Log:                   dh.Log,
		DG:                    dh.DG,
		NitradoV2ServiceToken: dh.NitradoV2ServiceToken,
		Guild:                 guild,
	}

	withoutPrefix := m.Content[len(guild.Bot.Prefix):]
	split := strings.Split(withoutPrefix, " ")
	commandName := split[0]

	var command utils.Command

	if val, ok := dh.Config.Bot.Commands[commandName]; ok {
		command = val
		commandHandler.Command = command
	} else {
		dh.Log.Log(fmt.Sprintf("ERROR: Command not found: %s", commandName), dh.Log.LogInformation)
		commandHandler.SendErrorMessage("ERROR: Command not found", fmt.Sprintf("Command not found: %s", commandName), "Please use the help command to find available commands", "", m.Content, m.ChannelID)
		return
	}

	var foundRole = false

	if val, ok := guild.Bot.Commands[command.Name]; ok {
		if len(val.Roles) == 0 {
			foundRole = true
		}

		for _, commandRole := range val.Roles {
			commandRoleString := strconv.FormatInt(commandRole, 10)
			for _, userRole := range m.Member.Roles {
				if commandRoleString == userRole {
					foundRole = true
					break
				}
			}

			if foundRole {
				break
			}
		}
	} else {
		commandHandler.SendErrorMessage("ERROR: Command not enabled", "This command is not enabled for your server", "Please contact <@268238111262113792> for more information.", "", m.Content, m.ChannelID)
		return
	}

	if !foundRole {
		commandHandler.SendErrorMessage("ERROR: Insufficient permissions", fmt.Sprintf("Insufficient permissions for command: %s", commandName), "You are not authorized to use this command.", "", m.Content, m.ChannelID)
		return
	}

	args := split[1:]

	switch commandName {
	case "help":
		commandHandler.HelpCommand(command, m, args)
	case "player":
		commandHandler.ListPlayersByNameCommand(command, m, args)
	case "ban":
		commandHandler.BanPlayerByNameCommand(command, m, args)
	case "unban":
		commandHandler.UnbanPlayerByNameCommand(command, m, args)
	case "servers":
		commandHandler.ListGameserversCommand(command, m, args)
	case "stop":
		commandHandler.StopGameserverCommand(command, m, args)
	case "restart":
		commandHandler.RestartGameserverCommand(command, m, args)
	case "banlist":
		commandHandler.BanlistCommand(command, m, args)
	default:
		// TODO: Send error message to Discord
		dh.Log.Log(fmt.Sprintf("Command not implemented: %s\n", commandName), dh.Log.LogLow)
		commandHandler.HelpCommand(command, m, args)
	}

	return
}
