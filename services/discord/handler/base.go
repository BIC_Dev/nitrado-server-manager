package handler

import (
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// DiscordHandler strict
type DiscordHandler struct {
	Config                *utils.Config
	Log                   *utils.Log
	DG                    *discordgo.Session
	NitradoV2ServiceToken string
	GuildConfig           *filestore.GuildConfig
}

// SetupHandlers func
func (dh *DiscordHandler) SetupHandlers() {
	dh.DG.AddHandler(dh.MessageCreate)
	dh.DG.AddHandler(dh.MessageReaction)
}

func getPrefix(message string, prefixLength int) string {
	if len(message) < prefixLength {
		return ""
	}

	return strings.ToLower(message[0:prefixLength])
}
