package runners

import (
	"fmt"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/gammazero/workerpool"
	"github.com/jinzhu/copier"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/commands"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/discord/discordapi"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/nitrado"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// PlayerLog struct
type PlayerLog struct {
	Gamertag  string `json:"gamertag"`
	Name      string `json:"name"`
	Message   string `json:"message"`
	Timestamp int64  `json:"timestamp"`
}

// AdminLog struct
type AdminLog struct {
	Name      string `json:"name"`
	Command   string `json:"command"`
	Timestamp int64  `json:"timestamp"`
}

// LogsError struct
type LogsError struct {
	Errs        []*utils.ServiceError
	Gameservers []filestore.Gameserver
}

// MaxPlayerLogEmbedFields int
const MaxPlayerLogEmbedFields int = 15

// MaxAdminLogEmbedFields int
const MaxAdminLogEmbedFields int = 10

// MaxLinesPerEmbedField int
const MaxLinesPerEmbedField int = 5

// LogsRunner func
func (r *Runner) LogsRunner() {
	r.Log.Log("PROCESS: Logs runner started", r.Log.LogInformation)

	ticker := time.NewTicker(time.Duration(r.Config.Runners.LogsInterval) * time.Second)

	wp := workerpool.New(r.Config.Runners.LogsWorkers)

	for range ticker.C {
		for _, guild := range r.GuildConfig.Guilds {
			r.Log.Log(fmt.Sprintf("PROCESS: Running logs for guild: %d", guild.Guild.ID), r.Log.LogInformation)
			for _, gameserver := range guild.Gameservers {
				if !(gameserver.ChatLog.Enabled || gameserver.AdminLog.Enabled) || (gameserver.ChatLog.ChannelID == 0 && gameserver.AdminLog.ChannelID == 0) {
					continue
				}

				// var aGameserver filestore.Gameserver
				// var aGuild filestore.Guild

				// copier.Copy(&aGameserver, &gameserver)
				// copier.Copy(&aGuild, &guild)

				// wp.Submit(func() {
				// 	r.LogsJob(aGuild, aGameserver)
				// })

				r.AddLogsJobToWorkerPool(wp, guild, gameserver)
			}
		}
	}
}

// AddLogsJobToWorkerPool func
func (r *Runner) AddLogsJobToWorkerPool(wp *workerpool.WorkerPool, guild filestore.Guild, gameserver filestore.Gameserver) {
	wp.Submit(func() {
		r.LogsJob(guild, gameserver)
	})
}

// LogsJob func
func (r *Runner) LogsJob(guild filestore.Guild, gameserver filestore.Gameserver) {
	r.Log.Log(fmt.Sprintf("PROCESS: Getting logs for gameserver: %d", gameserver.ID), r.Log.LogInformation)

	request := nitrado.NitradoRequest{
		Config:                r.Config,
		NitradoV2ServiceToken: r.NitradoV2ServiceToken,
	}

	var requestData []nitrado.RequestData

	requestData = append(requestData, nitrado.RequestData{
		Gameserver: gameserver,
	})

	responses, errors := request.GoRequest(request.GetGlobalLogs, requestData, nil)

	for _, err := range errors {
		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", err.Error.Message, err.Error.Error()), r.Log.LogLow)
	}

	var embeddablePlayerLogs []commands.EmbeddableField
	var embeddableAdminLogs []commands.EmbeddableField

	for _, resp := range responses {
		if resp.PlayerLogs != nil {
			linesCount := 0

			var prevPlayerLog *PlayerLog
			playerLogLen := len(resp.PlayerLogs)

			for i := 0; i < playerLogLen; i++ {
				if prevPlayerLog == nil {
					prevPlayerLog = &PlayerLog{
						Gamertag:  resp.PlayerLogs[i].Gamertag,
						Name:      resp.PlayerLogs[i].Name,
						Message:   resp.PlayerLogs[i].Message,
						Timestamp: resp.PlayerLogs[i].Timestamp,
					}
					linesCount = 1
				} else if resp.PlayerLogs[i].Gamertag == prevPlayerLog.Gamertag && linesCount < MaxLinesPerEmbedField {
					prevPlayerLog.Message += fmt.Sprintf("\n%s", resp.PlayerLogs[i].Message)
					linesCount++
				} else {
					var newPlayerLog PlayerLog
					copier.Copy(&newPlayerLog, &prevPlayerLog)
					embeddablePlayerLogs = append(embeddablePlayerLogs, &newPlayerLog)

					prevPlayerLog = &PlayerLog{
						Gamertag:  resp.PlayerLogs[i].Gamertag,
						Name:      resp.PlayerLogs[i].Name,
						Message:   resp.PlayerLogs[i].Message,
						Timestamp: resp.PlayerLogs[i].Timestamp,
					}
					linesCount = 1
				}

				if i+1 >= playerLogLen {
					embeddablePlayerLogs = append(embeddablePlayerLogs, prevPlayerLog)
				}
			}
		}

		if resp.AdminLogs != nil {
			linesCount := 0

			var prevAdminLog *AdminLog
			adminLogLen := len(resp.AdminLogs)

			for i := 0; i < adminLogLen; i++ {
				if prevAdminLog == nil {
					prevAdminLog = &AdminLog{
						Name:      resp.AdminLogs[i].Name,
						Command:   resp.AdminLogs[i].Command,
						Timestamp: resp.AdminLogs[i].Timestamp,
					}
					linesCount = 1
				} else if resp.AdminLogs[i].Name == prevAdminLog.Name && linesCount < MaxLinesPerEmbedField {
					prevAdminLog.Command += fmt.Sprintf("\n%s", resp.AdminLogs[i].Command)
					linesCount++
				} else {
					var newAdminLog AdminLog
					copier.Copy(&newAdminLog, &prevAdminLog)

					embeddableAdminLogs = append(embeddableAdminLogs, &newAdminLog)

					prevAdminLog = &AdminLog{
						Name:      resp.AdminLogs[i].Name,
						Command:   resp.AdminLogs[i].Command,
						Timestamp: resp.AdminLogs[i].Timestamp,
					}
					linesCount = 1
				}

				if i+1 >= adminLogLen {
					embeddableAdminLogs = append(embeddableAdminLogs, prevAdminLog)
				}
			}
		}
	}

	slicedPlayerLogs := SliceEmbedFields(embeddablePlayerLogs, MaxPlayerLogEmbedFields)
	slicedAdminLogs := SliceEmbedFields(embeddableAdminLogs, MaxAdminLogEmbedFields)

	if len(slicedPlayerLogs) == 0 {
		r.Log.Log(fmt.Sprintf("INFO: No player logs for gameserver ID: %d", gameserver.ID), r.Log.LogInformation)
	}

	if len(slicedAdminLogs) == 0 {
		r.Log.Log(fmt.Sprintf("INFO: No admin logs for gameserver ID: %d", gameserver.ID), r.Log.LogInformation)
	}

	var le LogsError

	for _, err := range errors {
		le.Errs = append(le.Errs, err.Error)
		le.Gameservers = append(le.Gameservers, err.Gameserver)
	}

	var embeddableErrors []commands.EmbeddableError

	if len(le.Errs) > 0 {
		embeddableErrors = append(embeddableErrors, &le)
	}

	playerLogEmbedParams := commands.EmbeddableParams{
		Title:       fmt.Sprintf("%s", gameserver.Name),
		Description: "Global chat log",
		Color:       0x17D34B,
		TitleURL:    r.Config.Bot.TitleURL,
		Footer:      "Retrieved",
	}

	adminLogEmbedParams := commands.EmbeddableParams{
		Title:       fmt.Sprintf("%s", gameserver.Name),
		Description: "Admin command log",
		Color:       0x17D34B,
		TitleURL:    r.Config.Bot.TitleURL,
		Footer:      "Retrieved",
	}

	chatLogChannelID := fmt.Sprintf("%d", gameserver.ChatLog.ChannelID)
	adminLogChannelID := fmt.Sprintf("%d", gameserver.AdminLog.ChannelID)

	for _, pl := range slicedPlayerLogs {
		embed := commands.CreateEmbed(playerLogEmbedParams, pl, embeddableErrors)

		go r.CreateLogPost(chatLogChannelID, "", embed, gameserver.ID)
	}

	for _, al := range slicedAdminLogs {
		embed := commands.CreateEmbed(adminLogEmbedParams, al, embeddableErrors)

		go r.CreateLogPost(adminLogChannelID, "", embed, gameserver.ID)
	}
}

// ConvertToEmbedField func
func (pl *PlayerLog) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	t := time.Unix(pl.Timestamp, 0)
	t = t.UTC()
	hms := fmt.Sprintf("%02d:%02d:%02d UTC", t.Hour(), t.Minute(), t.Second())

	field := &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("%s - %s (%s)", hms, pl.Name, pl.Gamertag),
		Value:  pl.Message,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (al *AdminLog) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	t := time.Unix(al.Timestamp, 0)
	t = t.UTC()
	hms := fmt.Sprintf("%02d:%02d:%02d UTC", t.Hour(), t.Minute(), t.Second())

	field := &discordgo.MessageEmbedField{
		Name:   fmt.Sprintf("%s - %s", hms, al.Name),
		Value:  al.Command,
		Inline: false,
	}

	return field, nil
}

// ConvertToEmbedField func
func (gle *LogsError) ConvertToEmbedField() (*discordgo.MessageEmbedField, *utils.ServiceError) {
	var gameservers []string

	for _, gs := range gle.Gameservers {
		gameservers = append(gameservers, gs.Name)
	}

	field := &discordgo.MessageEmbedField{
		Name:   "ERROR: Failed Retreiving Logs",
		Value:  "\u200b",
		Inline: false,
	}

	return field, nil
}

// SliceEmbedFields func
func SliceEmbedFields(embedFields []commands.EmbeddableField, maxSize int) [][]commands.EmbeddableField {
	var slicedEmbedFields [][]commands.EmbeddableField

	if len(embedFields) == 0 {
		return slicedEmbedFields
	}

	if len(embedFields) <= maxSize {
		slicedEmbedFields = append(slicedEmbedFields, embedFields)
		return slicedEmbedFields
	}

	var start int = 0
	var end int = maxSize
	var embedLen int = len(embedFields)

	for {
		if end >= embedLen {
			slicedEmbedFields = append(slicedEmbedFields, embedFields[start:embedLen])
			break
		}

		slicedEmbedFields = append(slicedEmbedFields, embedFields[start:end])
		start = end
		end += maxSize
	}

	return slicedEmbedFields
}

// CreateLogPost func
func (r *Runner) CreateLogPost(channelID string, content string, embed *discordgo.MessageEmbed, gameserverID int) {
	discord := discordapi.Discord{
		DG:  r.DG,
		Log: r.Log,
	}

	_, cpErr := discord.CreatePost(channelID, content, embed)

	if cpErr != nil {
		r.Log.Log(fmt.Sprintf("ERROR: Failed to post chat log for gameserver (%d): %s", gameserverID, cpErr.Error()), r.Log.LogLow)
	}

	return
}
