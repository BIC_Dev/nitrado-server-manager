package runners

import (
	"fmt"

	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// ErrorHandler func
func (r *Runner) ErrorHandler(e chan *utils.ServiceError) {
	for {
		err := <-e
		fmt.Printf("\n\nHANDLING ERROR\n\n")

		r.Log.Log(fmt.Sprintf("ERROR: %s: %s", err.GetMessage(), err.Error()), r.Log.LogLow)
	}
}
