package runners

import (
	"github.com/bwmarrin/discordgo"
	"gitlab.com/BIC_Dev/nitrado-server-manager/services/filestore"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
)

// Runner struct
type Runner struct {
	Config                *utils.Config
	Log                   *utils.Log
	DG                    *discordgo.Session
	NitradoV2ServiceToken string
	GuildConfig           *filestore.GuildConfig
}

// StartBackgroundProcesses func
func (r *Runner) StartBackgroundProcesses() {
	go r.OnlinePlayersRunner()
	go r.LogsRunner()
}
