package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// PotentialRestartGameserver struct
type PotentialRestartGameserver struct {
	MessageID      string `gorm:"varchar(50);index"`
	GameserverID   int    `gorm:"varchar(50)"`
	GameserverName string `gorm:"varchar(250)"`
	DiscordUserID  string `gorm:"varchar(50)"`
	gorm.Model
}

// TableName func
func (PotentialRestartGameserver) TableName() string {
	return "potential_restart_gameserver"
}

// Create adds a record to DB
func (t *PotentialRestartGameserver) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create potential restart gameserver entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GameserverID
func (t *PotentialRestartGameserver) GetLatest(DBStruct db.Interface) (PotentialRestartGameserver, *utils.ModelError) {
	var potentialRestartGameserver PotentialRestartGameserver

	result := DBStruct.GetDB().Where("message_id = ? AND discord_user_id = ?", t.MessageID, t.DiscordUserID).Last(&potentialRestartGameserver)

	if gorm.IsRecordNotFoundError(result.Error) {
		return potentialRestartGameserver, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find potential restart gameserver by message_id and discord_user_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return PotentialRestartGameserver{}, modelError
	}

	return potentialRestartGameserver, nil
}

// Delete soft deletes a potential restart gameserver in DB
func (t *PotentialRestartGameserver) Delete(DBStruct db.Interface) *utils.ModelError {

	result := DBStruct.GetDB().Delete(&t)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to delete potential restart gameserver in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}
