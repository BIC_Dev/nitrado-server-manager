package models

import (
	"net/http"

	"github.com/jinzhu/gorm"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils"
	"gitlab.com/BIC_Dev/nitrado-server-manager/utils/db"
)

// PotentialStopGameserver struct
type PotentialStopGameserver struct {
	MessageID      string `gorm:"varchar(50);index"`
	GameserverID   int    `gorm:"varchar(50)"`
	GameserverName string `gorm:"varchar(250)"`
	DiscordUserID  string `gorm:"varchar(50)"`
	gorm.Model
}

// TableName func
func (PotentialStopGameserver) TableName() string {
	return "potential_stop_gameserver"
}

// Create adds a record to DB
func (t *PotentialStopGameserver) Create(DBStruct db.Interface) *utils.ModelError {
	result := DBStruct.GetDB().Create(&t)

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to create potential stop gameserver entry in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}

// GetLatest gets the latest record for a GameserverID
func (t *PotentialStopGameserver) GetLatest(DBStruct db.Interface) (PotentialStopGameserver, *utils.ModelError) {
	var potentialStopGameserver PotentialStopGameserver

	result := DBStruct.GetDB().Where("message_id = ? AND discord_user_id = ?", t.MessageID, t.DiscordUserID).Last(&potentialStopGameserver)

	if gorm.IsRecordNotFoundError(result.Error) {
		return potentialStopGameserver, nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to find potential stop gameserver by message_id and discord_user_id in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return PotentialStopGameserver{}, modelError
	}

	return potentialStopGameserver, nil
}

// Delete soft deletes a potential stop gameserver in DB
func (t *PotentialStopGameserver) Delete(DBStruct db.Interface) *utils.ModelError {

	result := DBStruct.GetDB().Delete(&t)

	if gorm.IsRecordNotFoundError(result.Error) {
		return nil
	}

	if result.Error != nil {
		modelError := utils.NewModelError(result.Error)
		modelError.SetMessage("Unable to delete potential stop gameserver in DB")
		modelError.SetStatus(http.StatusBadRequest)
		return modelError
	}

	return nil
}
