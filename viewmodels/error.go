package viewmodels

// ErrorResponse response struct for an error
type ErrorResponse struct {
	StatusCode int    `json:"-"`
	Error      string `json:"error"`
	Message    string `json:"message"`
}

// NewErrorResponse creates a new error response
func NewErrorResponse(err string, message string) ErrorResponse {
	return ErrorResponse{
		Error:   err,
		Message: message,
	}
}
